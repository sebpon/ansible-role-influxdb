# Ansible role to deploy InfluxDB Open Source

This ansible role enables the deployment of influxdb oss on a CentOS / Redhat 7.x server.

## Dependencies

This role installs the following extra components 

- python package manager `pip`

- python package `requests`

- python package `influxdb`

These components are needed to be able to manage influxdb databases / users with the help of dedicated ansible modules.

## Usage

Here is a description of the most commonly used variables of this role.

`influxdb_role_action`: defines which actions will be run by the role.

`present`: the role will install/update (see influxdb_update) influxdb and configure it - default  
`install`: the role will only install/update (see influxdb_update) influxdb  
`configure`: the role will only configure influxdb

influxdb_update: defines if the role must update an existing influxdb installation

`false`: do not update - default  
`true`: do update. It will stop the influxdb service before installing the package.

> It must be combined with `influxdb_package_state = latest` instead of `present`

`influxdb_package_state`: defines the status of the influxdb package to install

`present`: will install the current version in influxdb repository. It does **NOT** update an existing installation - default  
`latest`: will install the latest current version available in influxdb  repository. It does update the installed version in case there is one.

## Playbooks

Sample playbook to install InfluxDB on a new server

```yaml
---
- hosts: influx
  become: yes

  roles:
    - ansible-role-influxdb
...
```

---

To update an existing installation without http authentication, use the following playbook

```yaml
---
- hosts: influx
  become: yes

  vars:
    influxdb_role_action: install
    influxdb_package_state: latest

  roles:
    - ansible-role-influxdb
...
```

## HTTP authentication

When `influxdb_http_auth_enabled` is set to `true`, the variables `influxdb_admin_username` and `influxdb_admin_password` must be filled otherwise the playbook will fail with the error message  

    Influxdb administrator username and/or password must not be empty!

---

Sample playbook with http authentication enabled

```yaml
---
- hosts: influx
  become: yes

  vars:
    influxdb_http_auth_enabled: true
    influxdb_admin_username: admin
    influxdb_admin_password: admin

  roles:
    - ansible-role-influxdb
...
```

---

Sample playbook to update an existing installation of influxdb with http authentication enabled

```yaml
---
- hosts: influx
  become: yes

  vars:
    influxdb_role_action: install
    influxdb_package_state: latest

  roles:
    - ansible-role-influxdb
...
```